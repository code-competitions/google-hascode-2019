#!/usr/bin/env bash

# Usage: ./run.sh METHOD (e.g. ./run.sh naive)
# writes all solutions + code.zip to output/
set -euo pipefail
METHOD="$1"

for FILE in $(ls input)
do
    echo $FILE
    ./main.py "$METHOD" "$FILE"
    echo
done

zip output/code.zip $(ag -l | grep -Ev '^input/')
