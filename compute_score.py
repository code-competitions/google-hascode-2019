def get_slide_tags(slide, photoset):
    try:
        (_, a), (_, b) = (photoset[i] for i in slide)
        tags = a.union(b)
    except TypeError:
        _, tags = photoset[slide]

    return tags


def validate_score(slideshow, photoset):
    indexes = set()
    for slide in slideshow:
        try:
            for i in slide:
                o, _ = photoset[i]
                if o != 'V' or i in indexes:
                    return False
                indexes.add(i)
        except TypeError:
            o, _ = photoset[slide]
            if photoset[slide][0] != 'H' or slide in indexes:
                return False
            indexes.add(slide)

    return True


def compute_slides_score(a, b, photoset):
    a = get_slide_tags(a, photoset)
    b = get_slide_tags(b, photoset)
    return min(len(a.intersection(b)), len(a-b), len(b-a))


def compute_slideshow_score(slideshow, photoset):
    assert validate_score(slideshow, photoset)
    score = 0
    for a, b in zip(slideshow, slideshow[1:]):
        score += compute_slides_score(a, b, photoset)

    return score
