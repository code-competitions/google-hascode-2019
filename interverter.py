from collections import defaultdict
import random
import compute_score
import utils

def triple_score_count(a, b, c, photos):
    return compute_score.compute_slides_score(a,b, photos) + compute_score.compute_slides_score(b,c, photos) 

# Naive solver
def solve(photos, name):
    save_index = 0
    # group photos by orientation
    orient = defaultdict(list)
    for i, (o, _) in enumerate(photos):
        orient[o].append(i)

    vertical = orient['V']
    vertical = [tuple(vertical[i:i+2]) for i in range(0, len(vertical), 2)]

    slideshow = orient['H'] + vertical
    index = 0
    
    while(True):
        random1 = random.randint(1, len(slideshow) - 2)
        random2 = random.randint(1, len(slideshow) - 2)

        first_first = triple_score_count(slideshow[random1 - 1], slideshow[random1], slideshow[random1 + 1], photos)
        first_second = triple_score_count(slideshow[random2 - 1], slideshow[random2], slideshow[random2 + 1], photos)


        second_first = triple_score_count(slideshow[random1 - 1], slideshow[random2], slideshow[random1 + 1], photos)
        second_second = triple_score_count(slideshow[random2 - 1], slideshow[random1], slideshow[random2 + 1], photos)

        if first_first + first_second < second_first + second_second:
            temp = slideshow[random1]
            slideshow[random1] = slideshow[random2]
            slideshow[random2] = temp
        index += 1
        if index % 100000 == 0:
            save_index = (save_index + 1) % 2
            print(save_index)
            print(compute_score.compute_slideshow_score(slideshow, photos))
            utils.save(slideshow, str(save_index) + '_' + name)
        

    return slideshow
