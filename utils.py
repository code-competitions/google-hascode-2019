import os

ROOT = os.path.dirname(os.path.realpath(__file__))


def load(name):
    path = os.path.join(ROOT, "input", name)
    photos = []
    with open(path) as f:
        next(f)  # skip 1st line (# of photos)
        for line in f:
            orient, _, tags = line.strip().split(" ", 2)
            tags = set(tags.split(" "))
            photos.append((orient, tags))

    return photos


def save(slideshow, name):
    path = os.path.join(ROOT, "output", name)
    with open(path, 'w') as f:
        f.write("{}\n".format(len(slideshow)))
        for slide in slideshow:
            try:
                slide = " ".join(map(str, slide))
            except TypeError:
                slide = str(slide)
            f.write("{}\n".format(slide))
