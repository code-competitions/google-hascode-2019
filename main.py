#!/usr/bin/env python3
import sys

import utils
import naive
import interverter
from compute_score import compute_slideshow_score

METHODS = dict(naive=naive, interverter=interverter)


def main(method, name):
    method = METHODS[method]
    photos = utils.load(name)
    slideshow = method.solve(photos, name)
    score = compute_slideshow_score(slideshow, photos)
    utils.save(slideshow, name)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
