from collections import defaultdict


# Naive solver
def solve(photos):
    # group photos by orientation
    orient = defaultdict(list)
    for i, (o, _) in enumerate(photos):
        orient[o].append(i)

    vertical = orient['V']
    vertical = [tuple(vertical[i:i+2]) for i in range(0, len(vertical), 2)]
    slideshow = orient['H'] + vertical

    return slideshow
